package com.assignment.carparkingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CarParkingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarParkingServiceApplication.class, args);
	}

}
