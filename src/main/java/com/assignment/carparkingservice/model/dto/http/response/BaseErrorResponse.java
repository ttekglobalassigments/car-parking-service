package com.assignment.carparkingservice.model.dto.http.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseErrorResponse {

  private HttpStatus status;
  private String message;
}
