package com.assignment.carparkingservice.model.dto.http.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarParkingRequestDTO {

  @JsonProperty("latitude")
  @NotNull(message = "latitude field cannot be null or empty")
  private Double latitude;

  @JsonProperty("longitude")
  @NotNull(message = "longitude field cannot be null or empty")
  private Double longitude;

  @JsonProperty("page")
  @Min(value = 1, message = "page value should be greater or equal to 1")
  private int page = 1;

  @JsonProperty("per_page")
  @Min(value = 1, message = "per page value should be greater or equal to 1")
  private int perPage = 50;
}
