package com.assignment.carparkingservice.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarParkItemDTO {

  @JsonProperty("timestamp")
  private String timestamp;
  @JsonProperty("carpark_data")
  private List<CarParkDataDTO> carParkData;
}
