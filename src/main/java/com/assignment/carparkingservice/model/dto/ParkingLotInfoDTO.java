package com.assignment.carparkingservice.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParkingLotInfoDTO {

  @JsonProperty("total_lots")
  private int totalLots;

  @JsonProperty("lot_type")
  private String lotType;

  @JsonProperty("lots_available")
  private int lotsAvailable;
}
