package com.assignment.carparkingservice.model.dto;

import com.assignment.carparkingservice.model.dao.CarPark;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarParkDTO {

  @JsonProperty("address")
  private String address;

  @JsonProperty("latitude")
  private double latitude;

  @JsonProperty("longitude")
  private double longitude;

  @JsonProperty("total_lots")
  private int totalParkingLots;

  @JsonProperty("available_lots")
  private int availableParkingLots;

  // Constructor to create a CarParkingInfoDTO from a CarParkingLot instance
  public CarParkDTO(CarPark carPark) {
    this.address = carPark.getAddress();
    this.latitude = carPark.getLongitude();
    this.longitude = carPark.getLatitude();
    this.totalParkingLots = carPark.getTotalLots();
    this.availableParkingLots = carPark.getAvailableLots();
  }
}
