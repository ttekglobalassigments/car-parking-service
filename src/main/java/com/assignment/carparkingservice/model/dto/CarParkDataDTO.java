package com.assignment.carparkingservice.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarParkDataDTO {

  @JsonProperty("carpark_number")
  private String carParkingNumber;

  @JsonProperty("update_datetime")
  private String updateDatetime;

  @JsonProperty("carpark_info")
  private List<ParkingLotInfoDTO> parkingLotInfoDTO;
}
