package com.assignment.carparkingservice.model.dao;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity(name = "car_park")
@Data
@Table(name = "car_park")
@AllArgsConstructor
@NoArgsConstructor
public class CarPark {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private UUID id;

  @Column(name = "car_park_no", nullable = false, unique = true)
  private String carParkNo;

  @Column(name = "address", nullable = false)
  private String address;

  @Column(name = "longitude", nullable = false)
  private Double longitude;

  @Column(name = "latitude", nullable = false)
  private Double latitude;

  @Column(name = "total_lots")
  private int totalLots;

  @Column(name = "available_lots")
  private int availableLots;

  @Column(name = "created_at", nullable = false, updatable = false)
  @CreationTimestamp
  private LocalDateTime createdAt;

  @Column(name = "updated_at")
  @UpdateTimestamp
  private LocalDateTime updatedAt;

  private transient Double distance;


  public CarPark(String carParkNo, String address, Map<String, Double> coordinates) {
    this.id = UUID.randomUUID();
    this.carParkNo = carParkNo;
    this.address = address;
    this.longitude = coordinates.get("latitude");
    this.latitude = coordinates.get("longitude");
    this.createdAt = LocalDateTime.now();
    this.updatedAt = LocalDateTime.now();
  }
}
