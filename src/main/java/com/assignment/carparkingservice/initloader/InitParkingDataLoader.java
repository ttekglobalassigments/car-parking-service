package com.assignment.carparkingservice.initloader;

import com.assignment.carparkingservice.exception.GenericException;
import com.assignment.carparkingservice.model.dao.CarPark;
import com.assignment.carparkingservice.service.CarParksService;
import com.assignment.carparkingservice.util.CoordinatesConversionUtil;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class InitParkingDataLoader {

  private final CarParksService carParksService;

  private final ResourceLoader resourceLoader;

  @EventListener
  public void appReady(ApplicationReadyEvent event) {
    loadParkingLotsSeedData();
  }

  private void loadParkingLotsSeedData() {
    if (!carParksService.findAll().isEmpty()) {
      log.debug("car park data already present.");
      return;
    }

    try (InputStream inputStream = resourceLoader.getResource(
        "classpath:static/HDBCarparkInformation.csv").getInputStream();
        CSVParser csvParser = CSVParser.parse(inputStream, StandardCharsets.UTF_8,
            CSVFormat.DEFAULT.withFirstRecordAsHeader())) {

      List<CarPark> carParkedList = new ArrayList<>();
      for (CSVRecord csvRecord : csvParser) {
        CarPark parkedCar = getCarPark(csvRecord);
        carParkedList.add(parkedCar);
      }
      log.debug("Successfully parsed CSV file data.");
      carParksService.saveAllCarParks(carParkedList);
    } catch (RuntimeException | IOException e) {
      log.error(String.format("Error occurred while loading car park seed data : %s", e));
      throw new GenericException(
          String.format("Error occurred while loading car park seed data : %s", e),
          HttpStatus.INTERNAL_SERVER_ERROR.value());
    }
  }

  private CarPark getCarPark(CSVRecord csvRecord) {
    String carParkName = csvRecord.get("car_park_no");
    String address = csvRecord.get("address").replace("\"", "");
    Map<String, Double> latLonCoordinate = CoordinatesConversionUtil.convertFromSVY21(
        Double.parseDouble(csvRecord.get("x_coord")),
        Double.parseDouble(csvRecord.get("y_coord")));
    return new CarPark(carParkName, address, latLonCoordinate);
  }
}