package com.assignment.carparkingservice.scheduler;

import com.assignment.carparkingservice.model.dto.CarParkAvailabilityDTO;
import com.assignment.carparkingservice.service.CarParksService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Service class responsible for scheduling and updating car parking data.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class UpdateParkingLotScheduler {

  private final CarParksService carParksService;
  private final RestTemplate restTemplate = new RestTemplate();

  @Value("${com.assignment.carparkingsystem.update}")
  private String parkingInfoUpdateEndpoint;

  /**
   * Scheduled method to fetch and update car parking data from an external API.
   */
  @Scheduled(cron = "${scheduled.job.cron.expression}")
  public void updateParkingLotAvailabilityScheduler() {
    log.info("car parking cron job has started");
    try {
      CarParkAvailabilityDTO carParkAvailabilityDTO = fetchCarParkData(
          parkingInfoUpdateEndpoint);
      if (carParkAvailabilityDTO != null) {
        carParksService.updateCarParkAvailability(carParkAvailabilityDTO);
      }
      log.debug("carParkingDataScheduler cron job finished");
    } catch (RuntimeException ex) {
      log.error(String.format("Scheduler was not able to complete its operations: %s", ex));
    }
  }

  private CarParkAvailabilityDTO fetchCarParkData(String apiUrl) {
    return restTemplate.getForObject(apiUrl, CarParkAvailabilityDTO.class);
  }
}
