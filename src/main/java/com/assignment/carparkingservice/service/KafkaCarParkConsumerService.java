package com.assignment.carparkingservice.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.support.Acknowledgment;

/**
 * Service interface for consuming Kafka events related to car parking updates.
 */
public interface KafkaCarParkConsumerService {

  /**
   * Consumes and processes the update car parking event from Kafka.
   *
   * @param consumerRecord The Kafka ConsumerRecord containing event details.
   * @param acknowledgment The Acknowledgment object for acknowledging event processing.
   */
  void consumeCarParkAvailabilityEvent(ConsumerRecord<String, String> consumerRecord,
      Acknowledgment acknowledgment);
}
