package com.assignment.carparkingservice.service.impl;

import static com.assignment.carparkingservice.util.DistanceCalcUtil.calculateDistance;

import com.assignment.carparkingservice.model.dao.CarPark;
import com.assignment.carparkingservice.model.dto.CarParkAvailabilityDTO;
import com.assignment.carparkingservice.model.dto.CarParkDTO;
import com.assignment.carparkingservice.model.dto.ParkingLotInfoDTO;
import com.assignment.carparkingservice.model.dto.http.request.CarParkingRequestDTO;
import com.assignment.carparkingservice.repository.CarParkRepository;
import com.assignment.carparkingservice.service.CarParksService;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CarParksServiceImpl implements CarParksService {

  private final CarParkRepository carParkRepository;

  @Override
  public List<CarParkDTO> findNearestCarParkingLots(
      CarParkingRequestDTO carParkingRequestDTO) {
    return carParkRepository.findAllAvailableParkingLots().stream()
        .map(carParkingLot -> {
          double distance = calculateDistance(carParkingRequestDTO.getLatitude(),
              carParkingRequestDTO.getLongitude(),
              carParkingLot.getLongitude(),
              carParkingLot.getLatitude());
          carParkingLot.setDistance(distance);
          return new AbstractMap.SimpleEntry<>(carParkingLot, distance);
        })
        .sorted(Comparator.comparingDouble(AbstractMap.SimpleEntry::getValue))
        .skip((carParkingRequestDTO.getPage() - 1) * carParkingRequestDTO.getPerPage())
        .limit(carParkingRequestDTO.getPerPage())
        .map(entry -> new CarParkDTO(entry.getKey()))
        .collect(Collectors.toList());
  }

  @Override
  public void saveAllCarParks(List<CarPark> carParks) {
    carParkRepository.saveAll(carParks);
    log.info("All parking lots saved/updated.");
  }

  @Override
  public CarPark findByCarParkNumber(String carParkNumber) {
    return carParkRepository.findOneByCarParkNo(carParkNumber);
  }

  @Override
  public List<CarPark> findAll() {
    return carParkRepository.findAll();
  }

  public synchronized void updateCarParkAvailability(CarParkAvailabilityDTO carParkAvailabilityDTO) {
    try {
      List<CarPark> carParks = new ArrayList<>();

      carParkAvailabilityDTO.getCarParkItemDTOS().forEach(item ->
          item.getCarParkData().forEach(carParkData -> {
            int availableLots = 0;
            int totalLots = 0;

            for (ParkingLotInfoDTO carParkInfo : carParkData.getParkingLotInfoDTO()) {
              availableLots += carParkInfo.getLotsAvailable();
              totalLots += carParkInfo.getTotalLots();
            }

            String carParkNumber = carParkData.getCarParkingNumber();
            CarPark carPark = carParkRepository.findOneByCarParkNo(carParkNumber);
            if (carPark != null) {
              carPark.setAvailableLots(availableLots);
              carPark.setTotalLots(totalLots);
              carParks.add(carPark);
            } else {
              log.error(String.format("Car parking lot not found with car park number %s",
                  carParkNumber));
            }
          }));
      saveAllCarParks(carParks);
    } catch (RuntimeException ex) {
      log.error(
          String.format("Error occurred while updating car park lot : %s", ex.getMessage()));
    }
  }
}
