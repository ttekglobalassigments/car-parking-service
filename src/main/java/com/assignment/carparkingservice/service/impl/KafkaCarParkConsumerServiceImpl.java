package com.assignment.carparkingservice.service.impl;

import com.assignment.carparkingservice.model.dto.CarParkAvailabilityDTO;
import com.assignment.carparkingservice.model.dto.CarParkDataDTO;
import com.assignment.carparkingservice.model.dto.CarParkItemDTO;
import com.assignment.carparkingservice.service.CarParksService;
import com.assignment.carparkingservice.service.KafkaCarParkConsumerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDateTime;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class KafkaCarParkConsumerServiceImpl implements KafkaCarParkConsumerService {

  private final CarParksService carParksService;

  @Override
  @KafkaListener(topics = {"${com.assignment.carparkingsystem.kafka.topic}"},
      containerFactory = "kafkaListenerContainerFactory", groupId = "car-parking")
  public void consumeCarParkAvailabilityEvent(ConsumerRecord<String, String> consumerRecord,
      Acknowledgment acknowledgment) {
    try {
      log.info("consumeCarParkLotUpdatedEvent received.");
      // Parse kafka string to kafka dto
      String kafkaEventDTOString = consumerRecord.value();
      if (!kafkaEventDTOString.isEmpty()) {
        CarParkDataDTO carParkData = new ObjectMapper().readValue(kafkaEventDTOString,
            CarParkDataDTO.class);
        CarParkItemDTO itemDetailDTO = new CarParkItemDTO(
            LocalDateTime.now().toString(),
            List.of(carParkData));
        CarParkAvailabilityDTO carParkAvailabilityDTO = new CarParkAvailabilityDTO(
            List.of(itemDetailDTO));
        carParksService.updateCarParkAvailability(carParkAvailabilityDTO);
        log.info("successfully updated car park data.");
        acknowledgment.acknowledge();
      } else {
        log.error("Received empty or invalid JSON.");
      }
    } catch (RuntimeException | JsonProcessingException ex) {
      log.error(String.format("car parking lot failed with %s", ex));
    }
  }
}
