package com.assignment.carparkingservice.service;

import com.assignment.carparkingservice.model.dao.CarPark;
import com.assignment.carparkingservice.model.dto.CarParkAvailabilityDTO;
import com.assignment.carparkingservice.model.dto.CarParkDTO;
import com.assignment.carparkingservice.model.dto.http.request.CarParkingRequestDTO;
import java.util.List;

/**
 * Service interface for managing car parking operations.
 */
public interface CarParksService {

  /**
   * Finds the nearest car parking lots based on the provided request parameters.
   *
   * @param carParkingRequestDTO The request parameters for the search.
   * @return A list of CarParkingInfoDTO objects containing details for the nearest car parking
   * locations.
   */
  List<CarParkDTO> findNearestCarParkingLots(CarParkingRequestDTO carParkingRequestDTO);

  /**
   * Saves or updates a list of car parking lots.
   *
   * @param carParks The list of car parking lots to be inserted or updated.
   */
  void saveAllCarParks(List<CarPark> carParks);

  /**
   * Retrieves details of a specific car parking lot based on its identifier.
   *
   * @param carParkNumber The unique identifier of the car parking lot.
   * @return The details of the specified car parking lot.
   */
  CarPark findByCarParkNumber(String carParkNumber);

  /**
   * Retrieves details of all car parking lots.
   *
   * @return A list of CarParkingLot objects representing details for all car parking lots.
   */
  List<CarPark> findAll();

  /**
   * Updates car parking data based on the provided event.
   *
   * @param carParkAvailabilityDTO The event containing car parking data to be updated.
   */
  void updateCarParkAvailability(CarParkAvailabilityDTO carParkAvailabilityDTO);
}