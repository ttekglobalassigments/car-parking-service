package com.assignment.carparkingservice.util;


public class DistanceCalcUtil {

  private static final double EARTH_RADIUS = 6371.0; // Assuming Earth radius in kilometers

  public static double calculateDistance(double startLat, double startLong, double endLat,
      double endLong) {
    double dLat = Math.toRadians(endLat - startLat);
    double dLong = Math.toRadians(endLong - startLong);

    double cosStartLat = Math.cos(Math.toRadians(startLat));
    double cosEndLat = Math.cos(Math.toRadians(endLat));

    double a = haversine(dLat) + cosStartLat * cosEndLat * haversine(dLong);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return EARTH_RADIUS * c;
  }

  private static double haversine(double angle) {
    return Math.pow(Math.sin(angle / 2), 2);
  }

}
