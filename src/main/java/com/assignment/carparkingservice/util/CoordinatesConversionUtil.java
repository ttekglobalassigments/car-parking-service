package com.assignment.carparkingservice.util;

import java.util.HashMap;
import java.util.Map;
import org.osgeo.proj4j.CRSFactory;
import org.osgeo.proj4j.CoordinateTransform;
import org.osgeo.proj4j.CoordinateTransformFactory;
import org.osgeo.proj4j.ProjCoordinate;

public class CoordinatesConversionUtil {

  // Convert SVY21 coordinates to WGS84 coordinates
  public static Map<String, Double> convertFromSVY21(double svy21X, double svy21Y) {
    CRSFactory factory = new CRSFactory();
    CoordinateTransformFactory ctf = new CoordinateTransformFactory();

    // Define coordinate reference systems
    org.osgeo.proj4j.CoordinateReferenceSystem svy21 = factory.createFromName("EPSG:3414");
    org.osgeo.proj4j.CoordinateReferenceSystem wgs84 = factory.createFromName("EPSG:4326");

    // Create coordinate transform
    CoordinateTransform transform = ctf.createTransform(svy21, wgs84);

    ProjCoordinate svy21Coordinates = new ProjCoordinate(svy21X, svy21Y);
    ProjCoordinate wgs84Coordinates = new ProjCoordinate();

    // Transform coordinates using Proj4J
    transform.transform(svy21Coordinates, wgs84Coordinates);

    double roundedLatitude = Math.round(wgs84Coordinates.y * 1e5) / 1e5;
    double roundedLongitude = Math.round(wgs84Coordinates.x * 1e3) / 1e3;

    Map<String, Double> coordinates = new HashMap<>();
    coordinates.put("longitude", roundedLongitude);
    coordinates.put("latitude", roundedLatitude);

    return coordinates;
  }
}