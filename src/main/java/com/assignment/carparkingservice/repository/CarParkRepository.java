package com.assignment.carparkingservice.repository;

import com.assignment.carparkingservice.model.dao.CarPark;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CarParkRepository extends JpaRepository<CarPark, UUID> {

  CarPark findOneByCarParkNo(String carParkNo);

  @Query("SELECT c FROM #{#entityName} c WHERE c.availableLots > 0")
  List<CarPark> findAllAvailableParkingLots();
}
