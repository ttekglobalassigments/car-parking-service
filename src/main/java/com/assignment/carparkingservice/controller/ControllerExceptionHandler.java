package com.assignment.carparkingservice.controller;

import com.assignment.carparkingservice.exception.GenericException;
import com.assignment.carparkingservice.model.dto.http.response.BaseErrorResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class ControllerExceptionHandler {

  /**
   * Handles GenericException and returns a ResponseEntity with appropriate status and error
   * message.
   *
   * @param ex The GenericException to be handled.
   * @return ResponseEntity containing the status and error message.
   */
  @ExceptionHandler(value = {GenericException.class})
  protected ResponseEntity<BaseErrorResponse> handleGenericException(GenericException ex) {
    HttpStatus httpStatus = HttpStatus.valueOf(ex.getStatusCode());
    String errorMessage = parseErrorMessage(ex.getMessage());
    return ResponseEntity.status(httpStatus).body(new BaseErrorResponse(httpStatus, errorMessage));
  }

  /**
   * Parses the error message from a JSON structure.
   *
   * @param errorMessage The error message in JSON format.
   * @return Parsed error message as a String.
   */
  private String parseErrorMessage(String errorMessage) {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      JsonNode jsonNode = objectMapper.readTree(errorMessage);
      if (jsonNode.has("error_debug")) {
        return jsonNode.get("error_debug").asText();
      } else if (jsonNode.has("error")) {
        return jsonNode.get("error").asText();
      }
    } catch (RuntimeException | JsonProcessingException e) {
      log.error("Error when parsing message: %s", e);
    }
    return errorMessage;
  }

  /**
   * Handles MethodArgumentNotValidException and returns a ResponseEntity with BAD_REQUEST status
   * and error message.
   *
   * @param ex The MethodArgumentNotValidException to be handled.
   * @return ResponseEntity containing the BAD_REQUEST status and error message.
   */
  @ExceptionHandler(MethodArgumentNotValidException.class)
  protected ResponseEntity<BaseErrorResponse> handleMethodArgumentNotValidException(
      MethodArgumentNotValidException ex) {
    String error = ex.getBindingResult().getFieldErrors().stream()
        .map(DefaultMessageSourceResolvable::getDefaultMessage)
        .collect(Collectors.toList()).get(0);
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
        .body(new BaseErrorResponse(HttpStatus.BAD_REQUEST, error));
  }
}
