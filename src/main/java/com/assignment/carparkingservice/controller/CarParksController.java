package com.assignment.carparkingservice.controller;

import com.assignment.carparkingservice.model.dto.CarParkDTO;
import com.assignment.carparkingservice.model.dto.http.request.CarParkingRequestDTO;
import com.assignment.carparkingservice.service.CarParksService;
import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/carparks")
@RequiredArgsConstructor
public class CarParksController {

  private final CarParksService carParksService;

  /**
   * Handles GET requests to retrieve the nearest parking lots based on the provided parameters.
   *
   * @param carParkingRequestDTO The validated request parameters for the search.
   * @return ResponseEntity with either the list of nearest car parking details or NO_CONTENT
   * status.
   */
  @GetMapping("/nearest")
  public ResponseEntity<Object> getNearestParkingLots(
      @ModelAttribute @Valid CarParkingRequestDTO carParkingRequestDTO) {
    List<CarParkDTO> response = carParksService.findNearestCarParkingLots(
        carParkingRequestDTO);
    return (response).isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
        : new ResponseEntity<>(response, HttpStatus.OK);
  }
}
