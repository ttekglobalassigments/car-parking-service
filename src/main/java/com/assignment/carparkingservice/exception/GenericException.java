package com.assignment.carparkingservice.exception;


public class GenericException extends RuntimeException {

  private int statusCode;

  public GenericException(String message, int statusCode) {
    super(message);
    this.statusCode = statusCode;
  }

  public int getStatusCode() {
    return statusCode;
  }
}

