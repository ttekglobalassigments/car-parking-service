package com.assignment.carparkingservice.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.assignment.carparkingservice.model.dto.CarParkDTO;
import com.assignment.carparkingservice.service.CarParksService;
import java.util.ArrayList;
import java.util.List;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {CarParksController.class})
@ExtendWith(SpringExtension.class)
public class CarParksControllerTest {

  MockHttpServletRequestBuilder requestBuilder;
  @Autowired
  private CarParksController carParkController;
  @MockBean
  private CarParksService carParkService;
  @MockBean
  private ControllerExceptionHandler controllerExceptionHandler;
  private List<CarParkDTO> carParkList;
  private static final String NEAREST_CAR_PARK_API = "/carparks/nearest?";

  @BeforeEach
  void setUp() {
    controllerExceptionHandler = new ControllerExceptionHandler();
    carParkList = new ArrayList<>(); // Initialize carParkList
    CarParkDTO carParkDto = new CarParkDTO("test", 1.0, 1.0, 1, 5);
    carParkList.add(carParkDto);
  }

  @Test
  void testGetNearestCarParks_When_AllQueriesAreValid_Then_ReturnOk() throws Exception {
    requestBuilder = MockMvcRequestBuilders.get(
            NEAREST_CAR_PARK_API)
        .param("latitude", "1.0").param("longitude", "1.0");
    when(carParkService.findNearestCarParkingLots(any())).thenReturn(carParkList);
    MockMvcBuilders.standaloneSetup(carParkController)
        .setControllerAdvice(controllerExceptionHandler)
        .build().perform(requestBuilder).andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
        .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(1)));
  }

  @Test
  void testGetNearestCarParks_When_AllQueriesAreValidAndResponseIsNull_Then_ReturnNoContent()
      throws Exception {
    List<CarParkDTO> carParkList = new ArrayList<>();
    requestBuilder = MockMvcRequestBuilders.get(
            NEAREST_CAR_PARK_API)
        .param("latitude", "1.0").param("longitude", "1.0");
    when(carParkService.findNearestCarParkingLots(any())).thenReturn(carParkList);
    MockMvcBuilders.standaloneSetup(carParkController)
        .setControllerAdvice(controllerExceptionHandler)
        .build().perform(requestBuilder).andExpect(MockMvcResultMatchers.status().isNoContent());
  }

  @ParameterizedTest
  @ValueSource(strings = {"null", "", "test"})
  void testGetNearestCarParks_When_LongitudeIsInvalidOrNull_Then_ReturnBadRequest(
      String longitude) throws Exception {
    requestBuilder = MockMvcRequestBuilders.get(NEAREST_CAR_PARK_API)
        .param("latitude", "1.0").param("longitude", longitude);
    MockMvcBuilders.standaloneSetup(carParkController)
        .setControllerAdvice(controllerExceptionHandler)
        .build().perform(requestBuilder)
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @ParameterizedTest
  @ValueSource(strings = {"null", "", "test"})
  void testGetNearestCarParks_When_LatitudeIsInvalidOrNull_Then_ReturnBadRequest(String latitude)
      throws Exception {
    requestBuilder = MockMvcRequestBuilders.get(NEAREST_CAR_PARK_API)
        .param("latitude", latitude).param("longitude", "1.0");
    MockMvcBuilders.standaloneSetup(carParkController)
        .setControllerAdvice(controllerExceptionHandler)
        .build().perform(requestBuilder)
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @ParameterizedTest
  @ValueSource(strings = {"null", "", "test"})
  void testGetNearestCarParks_When_PageIsInvalid_Then_ReturnBadRequest(String page)
      throws Exception {
    requestBuilder = MockMvcRequestBuilders.get(NEAREST_CAR_PARK_API)
        .param("latitude", "1.0").param("longitude", "1.0").param("page", page);
    MockMvcBuilders.standaloneSetup(carParkController)
        .setControllerAdvice(controllerExceptionHandler)
        .build().perform(requestBuilder)
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @ParameterizedTest
  @ValueSource(strings = {"null", "", "test"})
  void testGetNearestCarParks_When_PerPageIsInvalid_Then_ReturnBadRequest(String perPage)
      throws Exception {
    requestBuilder = MockMvcRequestBuilders.get(NEAREST_CAR_PARK_API)
        .param("latitude", "1.0").param("longitude", "1.0").param("perPage", perPage);
    MockMvcBuilders.standaloneSetup(carParkController)
        .setControllerAdvice(controllerExceptionHandler)
        .build().perform(requestBuilder)
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

}
