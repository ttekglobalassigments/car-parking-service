package com.assignment.carparkingservice.controller;

import com.assignment.carparkingservice.exception.GenericException;
import com.assignment.carparkingservice.model.dto.http.response.BaseErrorResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {ControllerExceptionHandler.class})
@ExtendWith(SpringExtension.class)
class ControllerExceptionHandlerTest {

  @InjectMocks
  private final ControllerExceptionHandler controllerExceptionHandler = new ControllerExceptionHandler();

  @Test
  void handleGenericException_shouldReturnErrorResponse() {
    // Arrange
    GenericException genericException = new GenericException("Sample error message", 500);
    ResponseEntity<BaseErrorResponse> responseEntity = controllerExceptionHandler.handleGenericException(genericException);
    // Assert
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
    assertEquals("Sample error message", responseEntity.getBody().getMessage());
  }

  @Test
  void handleMethodArgumentNotValidException_shouldReturnErrorResponse() {
    // Arrange
    MethodArgumentNotValidException validationException = mock(MethodArgumentNotValidException.class);
    BindingResult bindingResult = mock(BindingResult.class);

    // Simulate field error
    when(validationException.getBindingResult()).thenReturn(bindingResult);
    when(bindingResult.getFieldErrors()).thenReturn(
        Collections.singletonList(
            new FieldError("fieldName", "error.code", "Sample validation error")));

    // Act
    ResponseEntity<BaseErrorResponse> responseEntity = controllerExceptionHandler.handleMethodArgumentNotValidException(validationException);

    // Assert
    assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    assertEquals("Sample validation error", responseEntity.getBody().getMessage());
  }
}
