package com.assignment.carparkingservice.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.assignment.carparkingservice.model.dao.CarPark;
import com.assignment.carparkingservice.model.dto.CarParkAvailabilityDTO;
import com.assignment.carparkingservice.model.dto.CarParkDataDTO;
import com.assignment.carparkingservice.model.dto.CarParkItemDTO;
import com.assignment.carparkingservice.model.dto.ParkingLotInfoDTO;
import com.assignment.carparkingservice.service.impl.KafkaCarParkConsumerServiceImpl;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.web.reactive.function.client.WebClient;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class KafkaCarParkConsumerServiceImplTest {

  private KafkaCarParkConsumerServiceImpl kafkaConsumerServiceImpl;
  @Mock
  private Acknowledgment acknowledgment;

  @Mock
  private ConsumerRecord consumerRecord;

  @Mock
  private CarParksService carParkService;

  private WebClient webClient;

  CarParkAvailabilityDTO kafkaCarParksEventDTO = new CarParkAvailabilityDTO();
  CarPark carPark = new CarPark();

  @BeforeEach
  void setUp() {
    kafkaConsumerServiceImpl = new KafkaCarParkConsumerServiceImpl(carParkService);
    List<ParkingLotInfoDTO> carParkInfos = new ArrayList<>();
    carParkInfos.add(new ParkingLotInfoDTO(2, "C", 1));
    List<CarParkDataDTO> carParkData = new ArrayList<>();
    carParkData.add(new CarParkDataDTO("ASK", LocalDateTime.now().toString(), carParkInfos));
    List<CarParkItemDTO> carParkItemDTOS = new ArrayList<>();
    carParkItemDTOS.add(
        new CarParkItemDTO(LocalDateTime.now().toString(), carParkData));
    CarParkAvailabilityDTO kafkaCarParksEventDTO = new CarParkAvailabilityDTO(
        new ArrayList<>());
    kafkaCarParksEventDTO.setCarParkItemDTOS(carParkItemDTOS);
    carPark = new CarPark(UUID.randomUUID(), "Test", "test", 1.0, 1.0, 1, 5,
        LocalDateTime.now(), LocalDateTime.now(), 1.0);
  }

  @Test
  void testConsumeCarParkLotUpdatedEvent_When_EventDTOIsValid_Then_CallExpectedMethods() {
    doReturn(carPark).when(carParkService).findByCarParkNumber(any());
    doNothing().when(carParkService).saveAllCarParks(any());
    kafkaConsumerServiceImpl.consumeCarParkAvailabilityEvent(consumerRecord, acknowledgment);
    verify(carParkService, times(0)).saveAllCarParks(any());
  }
}
