package com.assignment.carparkingservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.assignment.carparkingservice.model.dao.CarPark;
import com.assignment.carparkingservice.model.dto.CarParkAvailabilityDTO;
import com.assignment.carparkingservice.model.dto.CarParkDTO;
import com.assignment.carparkingservice.model.dto.CarParkDataDTO;
import com.assignment.carparkingservice.model.dto.CarParkItemDTO;
import com.assignment.carparkingservice.model.dto.ParkingLotInfoDTO;
import com.assignment.carparkingservice.model.dto.http.request.CarParkingRequestDTO;
import com.assignment.carparkingservice.repository.CarParkRepository;
import com.assignment.carparkingservice.service.impl.CarParksServiceImpl;
import com.assignment.carparkingservice.util.CoordinatesConversionUtil;
import com.assignment.carparkingservice.util.DistanceCalcUtil;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {CarParksServiceImpl.class})
@ExtendWith(SpringExtension.class)
class CarParkServiceImplTest {

  @MockBean
  private CarParkRepository carParkRepository;

  @MockBean
  private CoordinatesConversionUtil coordinateConvertor;

  @MockBean
  DistanceCalcUtil distanceCalcUtil;

  @Autowired
  private CarParksServiceImpl carParkService;

  private List<CarPark> carParkList = new ArrayList<>();

  private CarParkingRequestDTO carParkQueryDto = new CarParkingRequestDTO();

  @BeforeEach
  void setUp() {
    carParkQueryDto = new CarParkingRequestDTO(1.0, 1.0, 1, 1);
    CarPark carPark = new CarPark(UUID.randomUUID(), "Test", "test", 1.0, 1.0, 1, 5,
        LocalDateTime.now(), LocalDateTime.now(), 1.0);
    carParkList.add(carPark);
    MockitoAnnotations.initMocks(this);
  }

  @Test
  void testGetNearestCarParks_When_AllQueriesAreValidAndReturnListIsEmpty_Then_CarParkDtoList() {
    when(carParkRepository.findAllAvailableParkingLots()).thenReturn(carParkList);
    List<CarParkDTO> carParkDtoList = carParkService.findNearestCarParkingLots(carParkQueryDto);
    assertEquals(1L, carParkDtoList.size());
  }

  @Test
  void testGetNearestCarParks_When_AllQueriesAreValidAndReturnListIsEmpty_Then_EmptyCarParkDtoList() {
    when(carParkRepository.findAllAvailableParkingLots()).thenReturn(new ArrayList<>());
    List<CarParkDTO> carParkDtoList = carParkService.findNearestCarParkingLots(carParkQueryDto);
    assertEquals(0L, carParkDtoList.size());
  }

  @Test
  void testFindAllCarParks_Return_ListCarParkDto() {
    when(carParkRepository.findAll()).thenReturn(carParkList);
    List<CarPark> carParkDtoList = carParkService.findAll();
    assertEquals(1, carParkDtoList.size());
  }

  @Test
  void testSaveAllCarParks_When_CarParkListIsValid_Return() {
    carParkService.saveAllCarParks(carParkList);
    verify(carParkRepository, times(1)).saveAll(any());
  }

  @Test
  void testFindByCarParkNumber_When_CarParkNumberExists_ReturnCarPark() {
    when(carParkRepository.findOneByCarParkNo(any())).thenReturn(carParkList.get(0));
    carParkService.findByCarParkNumber("carParkList");
    verify(carParkRepository, times(1)).findOneByCarParkNo(any());
  }


  @Test
  void updateCarParkingData_ShouldUpdateCarParks() {
    // Mock data for testing
    CarParkAvailabilityDTO carParkAvailabilityDTO = new CarParkAvailabilityDTO();
    CarParkDataDTO carParkData = new CarParkDataDTO();
    carParkData.setCarParkingNumber("A1");
    ParkingLotInfoDTO parkingLotInfoDTO = new ParkingLotInfoDTO();
    parkingLotInfoDTO.setLotsAvailable(10);
    parkingLotInfoDTO.setTotalLots(20);
    carParkData.setParkingLotInfoDTO(Collections.singletonList(parkingLotInfoDTO));
    CarParkItemDTO carParkingCarParkItemDTO = new CarParkItemDTO();
    carParkingCarParkItemDTO.setCarParkData(Collections.singletonList(carParkData));
    carParkAvailabilityDTO.setCarParkItemDTOS(Collections.singletonList(
        carParkingCarParkItemDTO));

    // Mock behavior
    Mockito.when(carParkRepository.findOneByCarParkNo("A1"))
        .thenReturn(new CarPark("A1", "test", Collections.emptyMap()));

    // Call the method to be tested
    carParkService.updateCarParkAvailability(carParkAvailabilityDTO);

    // Verify interactions
    Mockito.verify(carParkRepository, Mockito.times(1))
        .findOneByCarParkNo("A1");
    Mockito.verify(carParkRepository, Mockito.times(1))
        .saveAll(Mockito.anyList());
  }
}
