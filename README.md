# Car Parking System

The Car Parking System is a Spring Boot application designed to provide real-time updates on car parking information through external APIs, specifically handling data from a parking availability endpoint.

## Getting Started

### Prerequisites

Make sure you have the following installed (if not using docker compose):

- Java 17
- PostgreSQL database
- Apache Kafka (if using Kafka features)
- Zookeeper

## Approach and Assumptions

* On application initialisation, the function **updateParkingLotAvailabilityScheduler()** uploads all car parks seed data onto the database.

* In order to update car park availability status, we have added a schedular that extracts availability status from the the official gov site [car park availibity](https://api.data.gov.sg/v1/transport/carpark-availability) and updates for all car parks.

* The cron job updates the car park status in every 60 seconds.

* We have also kafka for asynchronous communication and update of the car park status. In this scenario we can only update a single car park status.

* We have used Haversine formula to calculate distance between coordinates as it seemed to be the most efficient and less commutation with respect to our requirements.

* We have used a coordinate converter class to convert SVY21 to latitude and longitude.

* In order to convert SVY21 to latitude and longitude format, I have used the **proj4j** library.

### Installation

1. **Clone the repository:**

   Using HTTPS:
   ```bash
   git clone https://gitlab.com/ttekglobalassigments/car-parking-service.git
   ```

   Using SSH:
   ```bash
   git clone git@gitlab.com:ttekglobalassigments/car-parking-service.git
   ```

   Navigate to the project directory and build the application using Maven.
   ```bash
   cd car-parking-service
   mvn clean install
   ```

## Run Docker Containers

Run the Docker Compose file to build and run all services.
```bash
docker-compose up --build
```

## Update Car Park Availability using Kafka

Send a request to Kafka Consumer. Open another terminal and run the following command from the Kafka Producer to send a request to the Kafka Consumer.
```bash
cat data.json | docker-compose exec -T kafka kafka-console-producer.sh --broker-list kafka:9092 --topic park_availability_topic
```

## Update Car Park availability using a scheduler

The system also incorporates a cron job scheduler tasked that retrieves real-time car park availability status from the official government website [car park availibity](https://api.data.gov.sg/v1/transport/carpark-availability). The scheduler is configured to update the availability status for all car parks at regular intervals of every 60 seconds.

## Get Nearest Car Parks

Use the following API to retrieve information on the nearest car parks from your location.

**API Endpoint:**
```
http://localhost:8081/carparks/nearest?latitude=1.37326&longitude=103.897&page=2&perPage=3
```

**Note:**
- Request parameters `latitude` and `longitude` are compulsory fields.
- `page` and `perPage` have default values of 1 and 50, respectively.

## Shut Down Application

To shut down the application and close all services, run the following command:
```bash
docker-compose down --volumes
```

# Car Parking System Project Structure

## 1. Main Package

The main package (`com.assignment.carparkingservice`) under the `src/main/java` directory contains the core components of the application.

## 2. Main Class
1. `CarParkingSystemApplication.java`: Main class responsible for running the Spring Boot application.

## 3. Configuration
1. `KafkaConsumerConfiguration.java`: Configuration class for Kafka Consumer settings.

## 4. Controller
1. `CarParksController.java`: Controller class handling HTTP requests related to car parking operations.
2. `ControllerExceptionHandler.java`: Global exception handler for handling exceptions across controllers.

## 5. Exception
1. `GenericException.java`: Custom exception class for generic exceptions.

## 6. Initialization Loader
1. `InitParkingDataLoader.java`: Class responsible for initializing parking data during application startup.

## 7. Model
### 7.1 DAO
1. `CarPark.java`: Represents the entity for a car parking lot.
### 7.2 DTO
1. Data Transfer Objects used for different purposes.

## 8. Repository
1. `CarParkRepository.java`: JPA repository interface for managing car parking lots.

## 9. Scheduler
1. `UpdateParkingLotScheduler.java`: Service class for scheduling periodic tasks.

## 10. Service
1. `CarParksService.java`: Interface defining car parking-related services.
2. `CarParksServiceImpl.java`: Implementation of car parking services.
### 10.1 Implementation
1. `KafkaCarParkConsumerService.java`: Interface for Kafka Consumer services.
2. `KafkaCarParkConsumerServiceImpl.java`: Implementation of Kafka Consumer services.

## 11. Utility
1. `CoordinatesConversionUtil.java`: Utility class for coordinate conversion.
2. `DistanceCalcUtil.java`: Utility class for distance calculation.